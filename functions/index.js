const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

function getRandom(arr, n) {
    var result = new Array(n),
        len = arr.length,
        taken = new Array(len);
    if (n > len)
        throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
}

exports.getQuizList = functions.https.onCall((data, context) => {
    const level = data.level;
    const topic = data.topic;
    return admin.firestore()
        .collection('quizzes')
        .where('level', '==', level)
        .where('topic_id', '==', topic)
        .get().then(querySnapshot => {
            var result = [];
            var docs = getRandom(querySnapshot.docs, 3);
            docs.forEach((doc) => {
                result.push(admin.firestore().doc(doc.ref.path));
            })
            return admin.firestore()
                .collection('test').doc('testdoc').update({ quizList: result });
        })
});
